.SILENT:
.PHONY: all clean current doc doc-srv serve tox


all:
	echo ""
	echo "  clean        Removes all temporary files"
	echo "  current      Runs the tox-environment for the current development"
	echo "  doc          Builds the documentation using 'Sphinx'"
	echo "  doc-srv      Serves the documentation on port 8082 (and automatically builds it)"
	echo "  serve        Runs the Django development server on port 8080"
	echo "  tox          Runs complete tox test"
	echo "  start-app    create new djangp app"
	echo "  pip-install  install the given 'dep' name and update the requirements file"
	echo "  git-work     take 'commit-msg' argument and commit the work branch than push \
	 the commit to the remote commit branch"
	echo ""


# deletes all temporary files created by Django
clean:
	find . -iname "*.pyc" -delete
	find . -iname "__pycache__" -delete

current:
	tox -q -e util

doc:
	tox -q -e doc

doc-srv: doc
	tox -q -e doc-srv

serve:
	tox -q -e run

tox:
	tox -q

start-app:
	echo "###################"; \
	echo "Creating '$(app_name)'"; \
	echo "###################"; \

	echo ""; \
	echo ""; \

	echo "Creating $(app_name)"; \
	read $(app_name); \
	cd ./apps; \
	python ../manage.py startapp $(app_name)

	echo ""; \
	echo ""; \

	echo "###################"; \
	echo "Done!"; \
	echo "###################";

pip-install:
	echo "###################"; \
	echo "Installing '$(dep)'"; \
	echo "###################"; \

	echo ""; \
	echo ""; \

	pipenv install $(dep); \
	echo "update requirements/development.txt"; \
	pipenv lock -r > requirements/development.txt; \

	echo ""; \
	echo ""; \

	echo "###################"; \
	echo "Done!"; \
	echo "###################"; \

git-work:
	echo "###################"; \
	echo "New commit ->  $(commit-msg)"; \
	echo "###################"; \

	echo ""; \
	echo ""; \

	# make file business login
	git add . ; \
	git commit -m "$(commit-msg)"; \
	git push origin work

	echo ""; \
	echo ""; \

	echo "###################"; \
	echo "Done!"; \
	echo "###################"; \
