# Python imports
from typing import Any, Sequence

# external imports
from factory import Faker, django, post_generation
from faker import Faker as FakerClass

# app imports
from ..models import Customer


class UserFactory(django.DjangoModelFactory):
    class Meta:
        model = Customer

    first_name = Faker("first_name")
    middle_name = Faker("name")
    phone_number = Faker("phone_number")
    surname = Faker("name")
    email = Faker("email")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):
        password = (
            extracted
            if extracted
            else FakerClass().password(
                length=30,
                special_chars=True,
                digits=True,
                upper_case=True,
                lower_case=True,
            )
        )
        self.set_password(password)

    # def save(self, *args, **kwargs):
    #     user = super().create(*args, **kwargs)
    #     user.is_active = True
    #     user.save()

    #     return user
