# Python imports
import email

# Django imports
from django.urls import reverse

# external imports
from faker import Faker
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

# app imports
from ..models import Customer
from .factory import UserFactory


class UserSignUpTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_object = UserFactory.build()
        cls.user_saved = UserFactory.create()
        cls.client = APIClient()
        cls.signup_url = reverse("register-api")
        cls.faker_obj = Faker()

    def test_if_data_is_correct_then_signup(self):
        # Prepare data
        signup_dict = {
            "email": self.user_object.email,
            "first_name": self.user_object.first_name,
            "middle_name": self.user_object.middle_name,
            "surname": self.user_object.surname,
            "password": "test_Pass",
            "password_confirmation": "test_Pass",
            "phone_number": self.user_object.phone_number,
        }

        # Make request
        response = self.client.post(self.signup_url, signup_dict)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Customer.objects.count(), 2)

        # Check database
        new_user = Customer.objects.get(email=self.user_object.email)

        print(new_user)
        self.assertEqual(new_user.email, self.user_object.email)
        self.assertEqual(new_user.phone_number, self.user_object.phone_number)

    def test_already_signed_up_user(self):
        # Prepare data
        signup_dict = {
            "email": self.user_saved.email,
            "first_name": self.user_saved.first_name,
            "middle_name": self.user_saved.middle_name,
            "surname": self.user_saved.surname,
            "password": "test_Pass",
            "password_confirmation": "test_Pass",
            "phone_number": self.user_saved.phone_number,
        }

        # Make request
        response = self.client.post(self.signup_url, signup_dict)

        # Check status response
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
