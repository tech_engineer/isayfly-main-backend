# Python imports
from base64 import b64encode

# Django imports
from django.urls import reverse

# external imports
import requests
import urllib3
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

# app imports
from ..models import Customer
from .factory import UserFactory


class UserLogOutTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_obj = UserFactory.build(password="asma3-meni-elsa3de")

        # generate a user object and activate it
        cls.user_saved = UserFactory.create(password="qwerty1234")
        cls.user_saved.is_active = True
        cls.user_saved.save()

        # initail the api client for making requests
        cls.client = APIClient()

        # define the login endpoint
        # example -> {current-domian}/api/auth/login
        cls.logout_url = reverse("logout-api")

    def test_logout_user(self):
        response = self.client.post(self.logout_url, headers=self.get_auth_header())

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def get_auth_header(self):
        headers = dict()

        encoded_credentials = b64encode(
            bytes(f'{"admin3@admin3.com"}:{"admin3"}', encoding="ascii")
        ).decode("ascii")
        auth_header = f"Basic {encoded_credentials}"

        print(auth_header)

        headers["Authorization"] = auth_header

        return headers
