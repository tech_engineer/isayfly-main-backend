# Django imports
from django.urls import reverse

# external imports
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

# app imports
from ..models import Customer
from .factory import UserFactory


class UserLogInTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user_obj = UserFactory.build(password="asma3-meni-elsa")

        # generate a user object and activate it
        cls.user_saved = UserFactory.create(password="qwerty1234")
        cls.user_saved.is_active = True
        cls.user_saved.save()

        # initail the api client for making requests
        cls.client = APIClient()

        # define the login endpoint
        # example -> {current-domian}/api/auth/login
        cls.login_url = reverse("login-api")

    def test_login_user_with_valid_credentials(self):
        credintails = {
            "email": self.user_saved.email,
            "password": "qwerty1234",
        }

        response = self.client.post(self.login_url, data=credintails)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_user_with_invalid_credentials(self):
        credintails = {"email": "gdsvfuvsdjfv", "password": "qwerty1234"}

        # login request
        response = self.client.post(self.login_url, data=credintails)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_uncorrect_password_and_email_fields(self):
        unvaild_pass_msg = "Password is incorrect."
        unvaild_email_msg = "Invalid email address."

        credintails = {
            "email": self.user_obj.email,
            "password": "qwer1234",
        }

        # login request for the unvaild email error
        response = self.client.post(self.login_url, data=credintails)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # compare the `unvaild_email_msg` variable and
        # the returned unvaild email message
        self.assertEqual(response.data["email"], unvaild_email_msg)

        # change the value of the `email` key to a vaild email
        credintails["email"] = self.user_saved.email

        # login request for the unvaild password error
        response = self.client.post(self.login_url, data=credintails)

        # compare the `unvaild_pass_msg` variable and
        # the returned unvaild password message
        self.assertEqual(response.data["password"], unvaild_pass_msg)
