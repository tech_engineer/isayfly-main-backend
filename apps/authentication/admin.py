# Django imports
from django.contrib import admin

# app imports
from .models import LoginDevice

admin.site.register(LoginDevice)
