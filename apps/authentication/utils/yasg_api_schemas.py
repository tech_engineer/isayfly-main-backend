# external imports
from drf_yasg import openapi

login_response_schema_dict = {
    "406": openapi.Response(
        description="Error response",
        examples={
            "application/json": {
                "status": "error",
                "status_code": "406",
                "message": "error_message",
            }
        },
    ),
    "201": openapi.Response(
        description="Success response",
        examples={
            "application/json": {
                "message": "",
                "status": {"code": 201, "message": "status_message"},
                "data": {"status": "", "message": "", "body": {}},
            }
        },
    ),
}
