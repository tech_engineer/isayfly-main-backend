# Django imports
# Django imports
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import check_password
from django.contrib.auth.password_validation import validate_password
from django.utils.translation import gettext_lazy as _

# external imports
import jwt
from customer.models import Customer
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed

# app imports
from lib.services import email


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(
        label=_("Password"),
        style={"input_type": "password"},
        trim_whitespace=False,
        write_only=True,
        required=True,
        validators=[validate_password],
    )

    def validate(self, data):
        """
        checks user exists and password is correct.
        """
        try:
            user = Customer.objects.get(email=data["email"])
        except Customer.DoesNotExist:
            raise serializers.ValidationError({"email": [_("Invalid email address.")]})

        if not check_password(data["password"], user.password):
            msg = _("Password is incorrect.")
            raise serializers.ValidationError({"password": [msg]}, code="authorization")

        authenticated_user = authenticate(
            request=self.context["request"],
            username=data["email"],
            password=data["password"],
        )

        if not authenticated_user:
            raise AuthenticationFailed("Invalid credentials, try again")
        else:
            login(request=self.context["request"], user=user)

        return data


class RegisterationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password_confirmation = serializers.CharField(write_only=True)

    class Meta:
        model = Customer
        fields = (
            "first_name",
            "middle_name",
            "surname",
            "email",
            "password",
            "password_confirmation",
            "phone_number",
        )

    def validate_email(self, email):
        print("Email: ", email)
        if Customer.objects.filter(email=email).exists():
            raise serializers.ValidationError(
                "This email alrady exists. Please use another email"
            )
        return email

    def validate(self, attrs):
        """check `password_confirmation` field if not equal `password` field"""

        if attrs["password_confirmation"] != attrs["password"]:
            raise serializers.ValidationError(
                {"message": "password_confirmation not equal password"}
            )
        return attrs

    def create(self, validated_data):
        del validated_data["password_confirmation"]

        # get `email` and password data from `validated_data`
        email = validated_data.pop("email")
        password = validated_data.pop("password")

        # create user instance
        instance = Customer.objects.create_user(email, password, **validated_data)

        return instance


class PasswordRestSerializer(serializers.Serializer):
    new_password = serializers.CharField(
        label=_("Password"),
        style={"input_type": "password"},
        trim_whitespace=False,
        write_only=True,
        required=True,
        validators=[validate_password],
    )
    confirm_password = serializers.CharField()

    def validate(self, attrs):
        if attrs["confirm_password"] != attrs["new_password"]:
            raise serializers.ValidationError("Passwords must match")
        return attrs


class ForgertPasswordRestSerializer(serializers.Serializer):
    token = serializers.CharField()
    new_password = serializers.CharField(
        label=_("Password"),
        style={"input_type": "password"},
        trim_whitespace=False,
        write_only=True,
        required=True,
        validators=[validate_password],
    )
    confirm_password = serializers.CharField()

    def validate_token(self, token_string):
        try:
            # decode the token using the django SECRET_KEY
            payload = jwt.decode(
                jwt=token_string, key=settings.SECRET_KEY, algorithms=["HS256"]
            )
        except jwt.ExpiredSignatureError as e:
            raise serializers.ValidationError("Activations link expired", code=400)
        except jwt.exceptions.DecodeError as e:
            raise serializers.ValidationError("Invalid Token", code=400)

        return payload

    def validate_confirm_password(self, conf_pswd):
        if conf_pswd != self.new_password:
            raise serializers.ValidationError("Passwords must match")
        return conf_pswd


class FindUserByEmailSerializer(serializers.Serializer):
    """
    This serializer will find user by email. If the user doesn't exist,
    raise validation error
    """

    user = None
    email_address = serializers.EmailField()

    def validate(self, attrs):
        try:
            self.user = Customer.objects.get(email=attrs["email_address"])
        except Customer.DoesNotExist:
            raise serializers.ValidationError(
                _("User with given email does not exists")
            )
        return super().validate(attrs)
