# Django imports
from django.urls import path

# app imports
from .views import (
    DeactivateAccount,
    ForgetPasswordFindByEmailView,
    ForgotPasswordResetView,
    LogInView,
    LogoutView,
    PasswordRestView,
    RegisterView,
    VerifyEmail,
)

urlpatterns = [
    path("sign-up", RegisterView.as_view(), name="register-api"),
    path("sign-in", LogInView.as_view(), name="login-api"),
    path("sign-out", LogoutView.as_view(), name="logout-api"),
    path("email-verify", VerifyEmail.as_view(), name="email-verify"),
    path("account-deactivate", DeactivateAccount.as_view(), name="account-deactivate"),
    path(
        "forget-password",
        ForgetPasswordFindByEmailView.as_view(),
        name="forget_password",
    ),
    path(
        "forget-password/confirm",
        ForgotPasswordResetView.as_view(),
        name="forget_password_confirm",
    ),
    path("reset-password", PasswordRestView.as_view(), name="reset_password"),
]
