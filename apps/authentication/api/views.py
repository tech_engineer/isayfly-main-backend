import json

# Django imports
from django.conf import settings
from django.contrib.auth import logout
from django.http import request

# external imports
import jwt
from customer.models import Customer
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

# external imports
from rest_framework_simplejwt.tokens import RefreshToken

# app imports
from lib.loggers.logger import Logger
from lib.services.email.email import (
    send_email_activation_link,
    send_email_deactivation_link,
    send_email_password_rest,
)
from lib.utils.api.response import CustomRenderer

# app imports
from ..models import LoginDevice
from ..utils.auth_helpers import (
    deactivate_account,
    delete_device,
    get_ip_address,
    get_user_agent,
    set_last_login,
)
from ..utils.yasg_api_schemas import login_response_schema_dict
from .exceptions import CustomErrorException
from .serializers import (
    FindUserByEmailSerializer,
    ForgertPasswordRestSerializer,
    LoginSerializer,
    PasswordRestSerializer,
    RegisterationSerializer,
)

init_logger = Logger("auth_logger")
logger = init_logger()


class RegisterView(generics.GenericAPIView):
    serializer_class = RegisterationSerializer
    # renderer_classes = [CustomRenderer]
    authentication_classes = []

    def post(self, request):
        """Register/Create new user"""

        data = request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        # get serialized email data
        email = serializer.data["email"]

        # get the new created user
        user = Customer.objects.get(email=email)

        # generate account access token
        token = RefreshToken.for_user(user).access_token

        # # send activation email
        # send_email_activation_link(user=user, request=self.request, email=email)

        return Response(
            # "Successfully registered. please check your email to activate your account",
            data={"token": str(token)},
            status=status.HTTP_201_CREATED
        )

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)


class LogInView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    # renderer_classes = [CustomRenderer, BrowsableAPIRenderer]
    authentication_classes = [BasicAuthentication]

    @swagger_auto_schema(responses=login_response_schema_dict)
    def post(self, request, *args, **kwargs):
        data = request.data
        login_serializer = self.get_serializer(data=data)

        if not login_serializer.is_valid():
            logger.error("Invalid credentials")
            raise CustomErrorException(
                # detail=login_serializer.errors,
                detail=self.serialize_error(login_serializer.errors),
                code="invaild_credentials",
                status_code=401,
            )

        user = Customer.objects.get(email=login_serializer.data.get("email"))
        self.login(request, user)

        logger.info("recieved post request")

        return Response("Successfully logged in", status=status.HTTP_200_OK)

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()

        return serializer_class(*args, **kwargs)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def login(self, request, user):
        ip_address = get_ip_address(request)
        device, os, browser = get_user_agent(request)

        login_device, _ = LoginDevice.objects.get_or_create(
            user=user, device=device, os=os, browser=browser, ip_address=ip_address
        )

        set_last_login(login_device)
        return login_device

    def serialize_error(self, errs):
        for err in errs:
            errs[err] = errs[err][0]
        return errs


class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = [BasicAuthentication]

    def post(self, request, *args, **kwargs):
        ip_address = get_ip_address(request)
        device, os, browser = get_user_agent(request)

        login_device = LoginDevice.objects.filter(
            user=request.user,
            device=device,
            os=os,
            browser=browser,
            ip_address=ip_address,
        ).first()
        delete_device(login_device)

        # logout the session user
        logout(request)

        print(request.headers)

        return Response(
            status=status.HTTP_204_NO_CONTENT, data="Successfully logged out."
        )


class VerifyEmail(APIView):
    """API View for email verification using JWT access token"""

    def get(self, request):
        """
        Verifies cutomer account

        Return:
            if token valid:
                (response): return 200 succuss response
            else:
                (exception): raise exception if the link is expired

        """
        token = request.GET.get("token")
        try:
            # decode the token using the django SECRET_KEY
            payload = jwt.decode(
                jwt=token, key=settings.SECRET_KEY, algorithms=["HS256"]
            )

            # get the customer instance by the extracted user_id from the token payload
            customer = Customer.objects.get(id=payload["user_id"])

            if not customer.is_active:
                customer.is_active = True
                customer.save()

            return Response("Successfully activated", status=status.HTTP_200_OK)
        except jwt.ExpiredSignatureError as e:
            return Response(
                "Activations link expired",
                status=status.HTTP_400_BAD_REQUEST,
            )
        except jwt.exceptions.DecodeError as e:
            return Response(
                {"error": "Invalid Token"}, status=status.HTTP_400_BAD_REQUEST
            )


class DeactivateAccount(APIView):
    # permission_classes = [IsAuthenticated]
    renderer_classes = [CustomRenderer, BrowsableAPIRenderer]

    def get(self, request, *args, **kwargs):
        """
        Verifies cutomer account

        Return:
            if token valid:
                (response): return 204 succuss response
            else:
                (exception): raise exception if the link is expired
        """
        if not self.request.user.is_active:
            return Response(
                "User already deactivated", status=status.HTTP_400_BAD_REQUEST
            )

        token = request.GET.get("token")
        try:
            # decode the token using the django SECRET_KEY
            payload = jwt.decode(
                jwt=token, key=settings.SECRET_KEY, algorithms=["HS256"]
            )

            # get the customer instance by the extracted user_id from the token payload
            customer = Customer.objects.get(id=payload["user_id"])

            # deactivate the customer account
            deactivate_account(customer)

            return Response(
                "Successfully deactivated", status=status.HTTP_204_NO_CONTENT
            )
        except jwt.ExpiredSignatureError as e:
            return Response(
                "Activations link expired",
                status=status.HTTP_400_BAD_REQUEST,
            )
        except jwt.exceptions.DecodeError as e:
            return Response("Invalid Token", status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        """
        Send deactivation email
        """
        # get user email
        user_email = request.user.email

        # send deactivation email
        send_email_deactivation_link(
            user=request.user, request=self.request, email=user_email
        )

        return Response(
            "Check your email to Confirm account deactivation",
            status=status.HTTP_200_OK,
        )

    def get_permissions(self):
        permissions = super().get_permissions()
        if self.request.method == "GET":
            return []
        return permissions


class ForgetPasswordFindByEmailView(APIView):
    """
    This endpoint accepts email_address. Then it sends an email to the user if
    the email is valid.

    Accepts:
        email_address: String

    Return
        status 200
    """

    serializer_class = FindUserByEmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        send_email_password_rest(
            serializer.user, request, serializer.data["email_address"]
        )
        return Response(status=200)


class PasswordRestView(APIView):
    permission_classes = [IsAuthenticated]
    renderer_classes = [CustomRenderer, BrowsableAPIRenderer]
    serializer_class = PasswordRestSerializer
    authentication_classes = [BasicAuthentication]

    def post(self, request, *args, **kwargs):
        """
        Set new password for user in the token
        """

        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = request.user
        user.set_password(serializer.validated_data["confirm_password"])
        user.save()

        return Response(
            "Successful Password reset",
            status=status.HTTP_200_OK,
        )


class ForgotPasswordResetView(APIView):
    serializer_class = ForgertPasswordRestSerializer

    def get(self, request, *args, **kwargs):
        # generate data for serializer
        data = {k: v for k, v in request.data.items()}
        data["token"] = request.GET.get("token")

        # serialize the given data
        serializer = self.serializer_class(data=data)
        serializer.is_valid(raise_exception=True)
        user = self.get_user(serializer.data["token"]["user_id"])

        # set new password
        user.set_password(serializer.validated_data["confirm_password"])
        user.save()

        return Response(
            "Successful Password reset",
            status=status.HTTP_200_OK,
        )

    def get_user(self, id):
        return Customer.objects.get(id=id)
