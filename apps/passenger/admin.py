# Django imports
from django.contrib import admin

# app imports
from .models import Passenger

admin.site.register(Passenger)
