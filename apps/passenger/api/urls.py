# Django imports
from django.urls import path

# app imports
from .views import (
    CustomerAPIView,
    PassengerAPIView,
    UpdateDeletePassengerAPIView,
)

urlpatterns = [
    path("", PassengerAPIView.as_view(), name="passenger-api"),
    path(
        "<int:id>",
        UpdateDeletePassengerAPIView.as_view(),
        name="passenger-update-delete-api",
    ),
    path("customer", CustomerAPIView.as_view(), name="customer-passenger-api"),
]
