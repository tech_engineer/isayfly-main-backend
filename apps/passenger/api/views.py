# Django imports
# Django imports
from django.conf import settings

# external imports
import jwt
from customer.models import Customer
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.authentication import JWTAuthentication

# app imports
from lib.utils.api.token_helpers import get_token_customer_id

# app imports
from ..models import Passenger
from .serializers import PassengerSerializer


class PassengerAPIView(APIView):
    serializer_class = PassengerSerializer

    def get_object(self, id):
        try:
            passenger_obj = Passenger.objects.get(id=id)
        except Passenger.DoesNotExist:
            return None
        return passenger_obj

    def post(self, request, *args, **kwargs):
        serailizer = self.serializer_class(data=request.data)

        if not serailizer.is_valid():
            return Response(data=serailizer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            # creates the passenger object
            serailizer.save()

        return Response("Successfully created", status=status.HTTP_201_CREATED)


class UpdateDeletePassengerAPIView(APIView):
    serializer_class = PassengerSerializer

    def get_object(self, id):
        try:
            passenger_obj = Passenger.objects.get(id=id)
        except Passenger.DoesNotExist:
            return None
        return passenger_obj

    def patch(self, request, *args, **kwargs):
        passenger_obj = self.get_object(kwargs["id"])

        if passenger_obj is None:
            return Response(
                "there is no passeneger with a such an idea",
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = self.serializer_class(instance=passenger_obj, data=request.data)

        if not serializer.is_valid():
            return Response(data=serializer.data, status=status.HTTP_400_BAD_REQUEST)

        return Response("Successfully updated", status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        passenger_obj = self.get_object(kwargs["id"])

        if passenger_obj is None:
            return Response(
                "There is no such a passenger in our DB",
                status=status.HTTP_400_BAD_REQUEST,
            )

        # delete the passenger object with the given `id`
        passenger_obj.delete()

        return Response("Successfully Deleted", status=status.HTTP_200_OK)


class CustomerAPIView(APIView):
    serializer_class = PassengerSerializer
    authenication_classes = [JWTAuthentication]

    def get_customer_obj(self, id):
        return Customer.objects.get(id=id)

    def get(self, request, *args, **kwargs):
        # get the token from the Authorization header
        token = request.headers.get("Authorization")[7:]

        # fetch user
        passengers = Passenger.objects.select_related("customer").filter(
            customer=self.get_customer_obj(get_token_customer_id(token))
        )
        serialized_objs = self.serializer_class(data=passengers, many=True)
        serialized_objs.is_valid()
        return Response(serialized_objs.data, status=status.HTTP_200_OK)
