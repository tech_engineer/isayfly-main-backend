# external imports
from customer.models import Customer
from rest_framework import serializers

# app imports
from ..models import Passenger


class PassengerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passenger
        fields = "__all__"

    def validate(self, attrs):
        if attrs["passport_date_of_issue"] > attrs["passport_date_of_expiry"]:
            raise serializers.ValidationError(
                "Expiry date can't be less than issue date"
            )
        return attrs

    def create(self, validated_data):
        return Passenger.objects.create(**validated_data)
