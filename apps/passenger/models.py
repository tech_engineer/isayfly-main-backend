# Django imports
# Django imports
from django.db import models
from django.utils.translation import gettext_lazy as _

# external imports
from address.models import Address
from customer.mixins import DateTimeMixin
from customer.models import Customer


class Passenger(DateTimeMixin):
    """
    Stores a Single Passenger entity, related to :model: 'Account'
    and :model: 'Address'
    """

    ADULT = "AD"
    CHILD = "CH"
    INFANT = "IN"

    PASSENGER_TYPE = [(ADULT, "Adult"), (CHILD, "Child"), (INFANT, "Infant")]
    GENDER = [("M", "Male"), ("F", "Female")]

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.SET_NULL, null=True)
    first_name = models.CharField(
        max_length=150, help_text="First name (as shown in ID)"
    )
    surname = models.CharField(max_length=150, help_text="Surname (as shown in ID)")
    passenger_type = models.CharField(
        _("passenger type"), max_length=20, choices=PASSENGER_TYPE
    )
    email = models.EmailField(_("email address"))
    gender = models.CharField(max_length=20, choices=GENDER)
    date_of_birth = models.DateField(_("Birth Day Date"))
    passport_id = models.CharField(_("Passport or ID number"), max_length=30)
    passport_date_of_issue = models.DateField(_("Password or ID date of issue"))
    passport_date_of_expiry = models.DateField(_("Password or ID date of expiry"))
