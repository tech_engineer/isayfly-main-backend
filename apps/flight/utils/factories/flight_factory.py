# Python imports
import datetime
from calendar import day_name
from datetime import timedelta
from uuid import uuid4

# external imports
from customer.models import Customer
from factory import Faker
from factory.django import DjangoModelFactory
from flight.models.flight import Flight


class FlightFactory(DjangoModelFactory):
    class Meta:
        model = Flight

    departure_airport = Faker("first_name")
    arrival_airport = Faker("last_name")
    flight_number = str(uuid4())
    departure_time = Faker("time_object")
    arrive_time = Faker("time_object")
    departure_date = datetime.datetime.now()
    arrive_date = datetime.datetime.now() - timedelta(days=100)
    customer = Customer.objects.get(id=37)
