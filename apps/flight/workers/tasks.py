# Python imports
from datetime import datetime, timedelta

# Django imports
from django.apps import apps
from django.db.models import DateTimeField, ExpressionWrapper, F

# external imports
from celery.app import task

# app imports
from core.celery import app
from lib.loggers.logger import Logger

_logger_initiator = Logger(
    "auth_worker_", logging_level="INFO", log_file_name="auth_worker.log"
)
logger = _logger_initiator()


@app.task(name="delete_flight_ids")
def delete_flight_ids():
    model = apps.get_model("flight.FlightId")

    flight_objects = model.objects.annotate(
        expires=ExpressionWrapper(
            F("created_at") + timedelta(hours=3), output_field=DateTimeField()
        )
    ).filter(expires__lte=datetime.now())
    logger.info(
        "fligth objects values: "
        + " ".join([str(id[0]) for id in list(flight_objects.values_list("id"))])
    )
    flight_objects.delete()

    return "it worked fine"
