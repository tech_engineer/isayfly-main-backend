# Django imports
from django.contrib import admin
from django.urls import include, path, re_path

# app imports
from .views import (
    FlightCustomerView,
    FlightFinalPriceView,
    FlightSearchView,
    FlightView,
)

urlpatterns = [
    path("search", FlightSearchView.as_view(), name="flight-api-search"),
    path("final-price", FlightFinalPriceView.as_view(), name="flight-api-final-price"),
    path("customer", FlightCustomerView.as_view(), name="flights-by-customer-api"),
    path("", FlightView.as_view(), name="fligth-api"),
]
