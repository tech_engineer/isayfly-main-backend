# Python imports
from datetime import date, datetime
from email.policy import default

# Django imports
from django.conf import settings
from django.utils.translation import gettext_lazy as _

# external imports
from rest_framework import serializers

# app imports
from ..models.flight import Flight, Ticket
from ..utils.constants.flight_const import TRAVELCLASS


class FlightSearchSerializer(serializers.Serializer):
    origin_location_code = serializers.CharField(
        label="city/airport IATA code from which the traveler will depart, e.g. BOS for Boston",
        required=True,
        error_messages={"required": "Please specify origin location"},
    )
    destination_location_code = serializers.CharField(
        label=_(
            "city/airport IATA code from which the traveler will depart, e.g. BOS for Boston"
        ),
        required=True,
        error_messages={"required": "Please specify destination location"},
    )
    departure_date = serializers.DateField(
        label=_(
            "the date on which the traveler will depart from the origin to go to the destination. \
            Dates are specified in the ISO 8601 YYYY-MM-DD format, e.g. 2017-12-25"
        ),
        required=True,
        input_formats=settings.DATE_INPUT_FORMATS,
        error_messages={
            "required": "Please specify the data with the correct format e.g. YYYY-MM-DD"
        },
    )
    return_date = serializers.DateField(
        label=_(
            "the date on which the traveler will depart from the destination to return to the origin. \
                Dates are specified in the ISO 8601 YYYY-MM-DD format, e.g. 2018-02-28"
        ),
        input_formats=settings.DATE_INPUT_FORMATS,
        required=False,
    )
    number_adults = serializers.IntegerField(
        label=_(
            "the number of adult travelers (age 12 or older on date of departure)."
        ),
        min_value=1,
        required=True,
        error_messages={"required": "Please specify the number of adult passengers"},
    )
    number_children = serializers.IntegerField(
        label=_(
            "the number of child travelers \
                (older than age 2 and younger than age 12 on date of departure)\
                     who will each have their own separate seat"
        ),
        required=False,
    )
    number_infants = serializers.IntegerField(
        label=_(
            "the number of infant travelers (whose age is less or equal to 2 on date of departure). \
                Infants travel on the lap of an adult traveler, \
                    and thus the number of infants must not exceed the number of adults."
        ),
        required=False,
    )
    travel_class = serializers.ChoiceField(
        label=_(
            "The accepted travel class is economy, premium economy, business or first class"
        ),
        choices=TRAVELCLASS,
        required=False,
    )
    non_stop = serializers.BooleanField(
        label=_(
            "if set to true, the search will find only flights going\
          es       from the origin to the destination with no stop in between"
        ),
        required=False,
    )
    max_price_per_traveler = serializers.IntegerField(
        label=_(
            "maximum price per traveler. By default, no limit is applied.\
            If specified, the value should be a positive number with no decimals"
        ),
        required=False,
    )
    currency_code = serializers.CharField(
        label=_("the preferred currency for the flight offers. e.g. EUR for Euro"),
        default="USD",
        required=False,
    )

    def validate_departure_date(self, dep_date):
        current_date = date.today()

        try:
            datetime.strptime(str(dep_date), "%Y-%m-%d")
        except ValueError:
            raise serializers.ValidationError(
                "This is the incorrect date string format. It should be YYYY-MM-DD"
            )

        if dep_date <= current_date:
            raise serializers.ValidationError("departure date not a valid date")

        return dep_date

    def validate(self, attrs):
        if "return_date" in attrs:
            if attrs["return_date"] <= attrs["departure_date"]:
                raise serializers.ValidationError(
                    "Return date can not before departure date or on the same day"
                )
        return attrs


class FlightSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flight
        fields = "__all__"


class TicketSerializer(serializers.ModelSerializer):
    flight_id = FlightSerializer(read_only=True)

    class Meta:
        model = Ticket
        fields = "__all__"
