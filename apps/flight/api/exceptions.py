# external imports
from rest_framework.exceptions import APIException


class FlightNotFound(APIException):
    """Raise a HTTP 404 error when Flight not found in the DB"""

    status_code = 404
    default_detail = "Flight Not Found"
    default_code = "flight_not_found"


class FlightQueryIdNotFound(APIException):
    """Raise a HTTP 404 error when Flight not found in the DB"""

    status_code = 404
    default_detail = "flight id not specified in the query params"
    default_code = "flight_query_id_not_found"


class NotIntegerType(APIException):
    """Raise a HTTP 404 error when Flight not found in the DB"""

    status_code = 404
    default_detail = "flight id must be in an integer form"
    default_code = "not_integer_id"
