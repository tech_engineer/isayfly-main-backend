# Python imports
import json
import uuid

# Django imports
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

# external imports
from amadeus import Client, ResponseError as AmadeusResponseError
from customer.models import Customer
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param
from rest_framework.views import APIView

# app imports
from lib.utils.api.token_helpers import get_token_customer_id
from lib.utils.caching.caching import Redis

# app imports
from ..models.cache_models import FlightId
from ..models.flight import Flight, Ticket
from ..utils.factories.flight_factory import FlightFactory
from .exceptions import FlightNotFound, FlightQueryIdNotFound, NotIntegerType
from .serializers import (
    FlightSearchSerializer,
    FlightSerializer,
    TicketSerializer,
)

amadeus = Client(
    client_id="oE4DpO5AR5TsQzZXA5mLnFREyBFm6T3Q",
    client_secret="HuI4Sk31LIaKGHMD",
)


class FlightSearchView(APIView):
    serializer_class = FlightSearchSerializer
    authentication_classes = []
    permission_classes = []

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.redis_class = Redis("localhost", 6379, db=0)

    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        limit = request.query_params.get("limit", 2)
        offset = request.query_params.get("offset", 1)

        data = request.data
        serializer = self.serializer_class(data=self.get_query_params)
        serializer.is_valid(raise_exception=True)
        serialized_data = serializer.data

        # check the exist
        if self.redis_class.has_cache_instance(**self.get_query_params):
            self.cacheid_obj = self.redis_class.get_cache_instance(
                **self.get_query_params
            )
            redis_key = self.redis_class.get_redis_key(self.cacheid_obj)

            if self.redis_class.has_redis_key(redis_key):
                data = self.redis_class.get_cached_instance(redis_key)
                paginated_data = self.paginator(json.loads(data), limit, offset)
                return Response(status=200, data=paginated_data)
        else:
            self.cacheid_obj = self.redis_class.get_cache_instance(
                **self.get_query_params
            )

        try:
            response = amadeus.shopping.flight_offers_search.get(
                **self.get_amadeus_params(serialized_data)
            )
            print("Hello before paginator")
            data = self.paginator(
                self.generate_flight_ids(response.data),
                limit=limit,
                offset=offset,
            )
            self.redis_class.set_redis_key(self.cacheid_obj, json.dumps(response.data))

        except AmadeusResponseError as error:
            res_error = error.response.result
            return Response(status=400, data={"errors": res_error})

        return Response(status=200, data=data)

    @property
    def get_query_params(self):
        serializer_fileds = [
            "origin_location_code",
            "destination_location_code",
            "departure_date",
            "return_date",
            "number_adults",
            "number_children",
            "number_infants",
            "travel_class",
            "non_stop",
            "max_price_per_traveler",
            "currency_code",
        ]
        query_params = self.request.query_params

        serializer_vars = dict()

        for param in query_params:
            if param in serializer_fileds:
                serializer_vars[param] = query_params.get(param)

        return serializer_vars

    @staticmethod
    def get_amadeus_params(params):
        amadeus_params = dict()

        if "origin_location_code" in params:
            amadeus_params["originLocationCode"] = params["origin_location_code"]

        if "destination_location_code" in params:
            amadeus_params["destinationLocationCode"] = params[
                "destination_location_code"
            ]

        if "departure_date" in params:
            amadeus_params["departureDate"] = params["departure_date"]

        if "return_date" in params:
            amadeus_params["returnDate"] = params["return_date"]

        if "number_adults" in params:
            amadeus_params["adults"] = params["number_adults"]

        if "number_children" in params:
            amadeus_params["children"] = params["number_children"]

        if "number_infants" in params:
            amadeus_params["infants"] = params["number_infants"]

        if "travel_class" in params:
            amadeus_params["travelClass"] = params["travel_class"]

        if "non_stop" in params:
            amadeus_params["nonStop"] = params["non_stop"]

        if "max_price_per_traveler" in params:
            amadeus_params["maxPrice"] = params["max_price_per_traveler"]

        if "currency_code" in params:
            amadeus_params["currencyCode"] = params["currency_code"]

        return amadeus_params

    def paginator(self, data, limit, offset):
        try:
            paginator = Paginator(data, limit)
        except:
            paginator = Paginator(data, limit)

        try:
            results = paginator.page(offset)
        except PageNotAnInteger:
            results = paginator.page(offset)
        except EmptyPage:
            results = []

        api_count = paginator.count
        try:
            api_next = (
                None
                if not results.has_next()
                else replace_query_param(
                    self.request.get_full_path(),
                    "offset",
                    str(results.next_page_number()),
                )
            )
            api_previous = (
                None
                if not results.has_previous()
                else replace_query_param(
                    self.request.get_full_path(),
                    "offset",
                    str(results.previous_page_number()),
                )
            )
        except AttributeError:
            api_next = None
            api_previous = None

        data = {
            "count": api_count,
            "next": api_next,
            "previous": api_previous,
            "results": list(results),
        }

        return data

    def generate_flight_ids(self, data):
        flights_json_objs = list(data)

        for fligth_json_obj in flights_json_objs:
            flight_id = str(uuid.uuid4())
            fligth_json_obj["flight_id"] = flight_id
            FlightId.objects.create(flight_id=flight_id, cache=self.cacheid_obj)
        return flights_json_objs


class FlightFinalPriceView(APIView):
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.redis_class = Redis("localhost", 6379, db=0)

    def post(self, request, *args, **kwargs):
        flight_id_query_param = request.query_params.get("flight_id")
        flight_obj = self.get_flight(flight_id_query_param)

        if flight_obj is None:
            return Response({"errors": ["No match Flight ID in the DB"]}, status=404)

        response = amadeus.shopping.flight_offers.pricing.post(flight_obj)
        return Response(data=response.data, status=200)

    def get_flight(self, flight_id):
        try:
            flight_obj = FlightId.objects.get(flight_id=flight_id)
        except FlightId.DoesNotExist:
            return None

        flights_json = self.redis_class.get_cached_instance(str(flight_obj.cache))

        for flight in json.loads(flights_json):
            if flight["flight_id"] == str(flight_id):
                return flight
        return None


class FlightCustomerView(APIView):
    serializer_class = FlightSerializer

    def get_customer_obj(self, id):
        return Customer.objects.get(id=id)

    def get(self, request, *args, **kwargs):
        # get the token from the Authorization header
        token = request.headers.get("Authorization")[7:]
        # fetch flights related to the customer
        flights = Flight.objects.filter(
            customer=self.get_customer_obj(get_token_customer_id(token))
        )
        serialized_objs = self.serializer_class(flights, many=True)
        return Response(serialized_objs.data, status=status.HTTP_200_OK)


class FlightView(APIView):
    serializer_class = FlightSerializer

    def get(self, request, *args, **kwargs):
        """Serialize the requested flight object"""

        flight_object = self.get_flight_object
        serializer = self.serializer_class(instance=flight_object)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        """Create new flight object"""

        request_data = request.data
        serializer = self.serializer_class(data=request_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("Succussfully created", status=status.HTTP_201_CREATED)

    def patch(self, request, *args, **kwargs):
        """Partially update a flight object"""

        flight_obj = self.get_flight_object
        request_data = request.data
        serializer = self.serializer_class(
            instance=flight_obj, data=request_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response("Succussfully Updated", status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        """Delete a flight object"""

        flight_obj = self.get_flight_object
        flight_obj.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

    @property
    def get_flight_object(self):
        """Get the Flight with given id else raise FlightNotFound exception"""
        try:
            return Flight.objects.get(id=self.get_flight_query_id)
        except Flight.DoesNotExist:
            raise FlightNotFound

    @property
    def get_flight_query_id(self):
        """Get the Flight id with given id else raise FlightNotFound exception"""

        get_request = self.request.GET
        flight_id = None

        if "flight_id" not in get_request:
            raise FlightQueryIdNotFound

        if get_request["flight_id"] == "":
            raise FlightQueryIdNotFound

        try:
            flight_id = int(get_request["flight_id"])
        except ValueError:
            raise NotIntegerType

        return flight_id
