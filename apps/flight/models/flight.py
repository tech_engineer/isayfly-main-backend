# Django imports
# Django imports
from django.db import models
from django.db.models.deletion import CASCADE, SET_NULL
from django.utils.translation import gettext_lazy as _

# external imports
from customer.models import Customer
from passenger.models import Passenger

# from .sub_models import FlightId


class Flight(models.Model):
    """
    Stores a single ticket entity.
    """

    departure_airport = models.CharField(_("Departure airport"), max_length=60)
    arrival_airport = models.CharField(_("arrival airport"), max_length=50)
    flight_number = models.CharField(_("Flight number"), max_length=20)
    departure_time = models.TimeField(_("Departure Time"))
    departure_date = models.DateField(_("Departure Date"))
    arrive_time = models.TimeField(_("Arrive Time"))
    arrive_date = models.DateField(_("Arrive Date"))
    customer = models.ForeignKey(Customer, on_delete=SET_NULL, null=True)

    def __str__(self) -> str:
        return "Flight Number: " + self.flight_number

    class Meta:
        app_label = "flight"
        verbose_name = "Flight"
        verbose_name_plural = "Flights"


class Ticket(models.Model):
    """
    Stores a single ticket entity

    `` constrains ``
    `` Relations ``
    """

    TICKET_TYPES = [("STD", "Standard"), ("BS", "Business"), ("1ST", "First")]
    flight = models.ForeignKey(Flight, on_delete=CASCADE)
    passenger_id = models.ForeignKey(Passenger, on_delete=CASCADE)
    user_id = models.ForeignKey(Customer, on_delete=SET_NULL, null=True)
    ticket_type = models.CharField(
        _("Ticket Type"), choices=TICKET_TYPES, max_length=30
    )
    seat_number = models.CharField(_("Seat number"), max_length=10)

    class Meta:
        app_label = "flight"
        verbose_name = "Ticket"
        verbose_name_plural = "Tickets"
