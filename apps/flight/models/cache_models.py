# Python imports
import uuid

# Django imports
from django.db import models

# app imports
from ..utils.constants.flight_const import TRAVELCLASS


class CacheId(models.Model):
    """
    Model to store the different Search API parameters,
    and use the generated UUID as redis cache key.
    """

    cache_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    origin_location_code = models.CharField(max_length=4)
    destination_location_code = models.CharField(max_length=4)
    departure_date = models.DateField()
    return_date = models.DateField(null=True, blank=True)
    number_adults = models.IntegerField(null=True, blank=True)
    number_children = models.IntegerField(null=True, blank=True)
    number_infants = models.IntegerField(null=True, blank=True)
    travel_class = models.CharField(max_length=20, null=True, blank=True)
    non_stop = models.BooleanField(null=True, blank=True)
    max_price_per_traveler = models.IntegerField(null=True, blank=True)
    currency_code = models.IntegerField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self, *args, **kwargs) -> None:
        return str(self.cache_id)

    class Meta:
        verbose_name = "Cache Id"
        verbose_name_plural = "Cache Ids"
        app_label = "flight"


class FlightId(models.Model):
    """
    Model to detect the requested flights and their related cache objects
    """

    flight_id = models.CharField(max_length=90)
    cache = models.ForeignKey(CacheId, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.flight_id

    class Meta:
        verbose_name = "Flight Id"
        verbose_name_plural = "Flight Ids"
        app_label = "flight"
