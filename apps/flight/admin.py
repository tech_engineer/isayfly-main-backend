# Django imports
from django.contrib import admin

# app imports
from .models.cache_models import CacheId, FlightId
from .models.flight import Flight, Ticket

admin.site.register(CacheId)
admin.site.register(FlightId)
admin.site.register(Flight)
admin.site.register(Ticket)
