# Django imports
# Django imports
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _

# external imports
from customer.managers import CustomUserManager


class CustomUser(PermissionsMixin, AbstractBaseUser):
    """
    Abstract User model that defines comman attributes
    that can inhertited by other models. This model uses email
    inside of username for login and other authentication layers.
    """

    email = models.EmailField(_("email address"), unique=True)
    is_active = models.BooleanField(_("Account Activation status"), default=False)
    is_frozen = models.BooleanField(_("Account frozen status"), default=False)
    is_staff = models.BooleanField(_("Is Staff"), default=False)

    USERNAME_FIELD = "email"

    objects = CustomUserManager()

    class Meta:
        abstract = True
