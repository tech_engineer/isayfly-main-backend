# Django imports
# Django imports
from django.db import models
from django.utils.translation import gettext_lazy as _

# external imports
from customer.models import Customer


class Address(models.Model):
    user_id = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name="user")
    address_1 = models.CharField(_("address 1"), max_length=128)
    address_2 = models.CharField(_("address 2"), max_length=128, blank=True)
    zip_code = models.CharField(_("zip code"), max_length=5, default="43701")
    country = models.CharField(_("Country"), max_length=64)
    state = models.CharField(_("State"), max_length=120)

    def __str__(self) -> str:
        return f"Address of {self.user_id.full_name}"
