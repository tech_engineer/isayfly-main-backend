# Django imports
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# app imports
from .models import Customer


class CustomUserAdmin(UserAdmin):
    model = Customer
    list_display = ("id", "email", "first_name", "surname", "is_active", "is_frozen")
    list_filter = (
        "email",
        "is_frozen",
        "is_active",
    )
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        ("Permissions", {"fields": ("is_frozen", "is_active")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2", "is_frozen", "is_active"),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)


admin.site.register(Customer, CustomUserAdmin)
