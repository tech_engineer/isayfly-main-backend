"""
    models.py

    Info about file:

"""
# Django imports
from django.db import models
from django.utils.translation import gettext_lazy as _

# app imports
from apps.base.model.user import CustomUser
from lib.constants.const import Const

# app imports
from .mixins import DateTimeMixin


class Customer(DateTimeMixin, CustomUser):
    """
    Customer model defines basic user attributes for login and signup
    """

    first_name = models.CharField(
        max_length=150, help_text="First name (as shown in ID)"
    )
    middle_name = models.CharField(
        max_length=150, help_text="Middle name (as shown in ID)"
    )
    surname = models.CharField(max_length=150, help_text=("Surname (as shown in ID)"))
    phone_number = models.CharField(max_length=40, blank=True)

    REQUIRED_FIELDS = ["first_name", "surname"]

    class Meta:
        verbose_name = "Customer"
        verbose_name_plural = "Customers"

    def __str__(self):
        return self.email

    @property
    def full_name(self):
        return self.first_name + " " + self.surname
