# external imports
from customer.models import Customer
from rest_framework import generics, mixins, status, viewsets
from rest_framework.response import Response

# app imports
from .exceptions import CustomerNotFound, NotCorrectParameters
from .serializers import CustomerSerializer, CustomerUpdateModelSerializer


class CustomerAPIView(
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    generics.RetrieveAPIView
):
    """
    Create, Update, Retrieve and Delete Customer instance.
    """

    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    
    def get_serializer_class(self):
        if self.request.method == "PATCH" or self.request.method == "PUT":
            self.serializer_class = CustomerUpdateModelSerializer
        return self.serializer_class

    def get_object(self):
        id_ = self.request.GET.get("id", None)

        if id_ is not None:
            try:
                instance = Customer.objects.get(id=id_)
                self.check_object_permissions(self.request, instance)
            except:
                raise CustomerNotFound
            return instance
        raise NotCorrectParameters

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    # def post(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_create(serializer)
    #     headers = self.get_success_headers(serializer.data)
    #     return Response(
    #         "Customer account has been successfully created",
    #         status=status.HTTP_201_CREATED,
    #         headers=headers,
    #     )

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.serializer_class(
            data=request.data, partial=True, instance=instance
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)
