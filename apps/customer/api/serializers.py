# Django imports
# Django imports
from django.contrib.auth.password_validation import validate_password

# external imports
from customer.models import Customer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator


class CustomerSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True, validators=[UniqueValidator(queryset=Customer.objects.all())]
    )
    password1 = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(write_only=True)

    class Meta:
        model = Customer
        fields = (
            "id",
            "first_name",
            "middle_name",
            "surname",
            "email",
            "password1",
            "password2",
        )

    def validate(self, attrs):
        if attrs["password1"] != attrs["password2"]:
            raise serializers.ValidationError(
                {"password": "password1 not equal password2"}
            )

        return attrs

    def create(self, validated_data):
        del validated_data["password2"]

        password = validated_data.pop("password1")
        email = validated_data.pop("email")

        instance = Customer.objects.create_user(email, password, **validated_data)

        return instance

    def update(self, instance, validated_data, *args, **kwargs):
        # instance.email = validated_data.get("email", instance.email)
        # instance.first_name = validated_data.get("first_name", instance.first_name)
        # instance.middle_name = validated_data.get("middle_name", instance.middle_name)
        # instance.surname = validated_data.get("surname", instance.surname)
        # instance.phone_number = validated_data.get(
        #     "phone_number", instance.phone_number
        # )
        # instance.save()
        # return instance
        return super().update(instance, validated_data, *args, **kwargs)


class CustomerUpdateModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ("first_name", "middle_name", "surname", "phone_number", "email")
        extra_kwargs = {"email": {"read_only": True}}
