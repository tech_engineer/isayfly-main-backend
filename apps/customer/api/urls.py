# Django imports
from django.urls import path

# app imports
from .views import CustomerAPIView

urlpatterns = [
    path("customer", CustomerAPIView.as_view(), name="customer-api"),
]
