# Django imports
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse

# external imports
from rest_framework_simplejwt.tokens import RefreshToken

# app imports
from .utils.email_threads import EmailThread


def send_mail(subject, email_body, to):
    # initiate an email thread
    EmailThread(subject=subject, recipient=to, html_content=email_body).start()


def send_email_activation_link(user, request, email):
    """
    Sends activation email to the new signing user

    Arguments:
        email (str): the new sign up user email

    Return:
        bool: pass

    """

    # generate account activation token
    token = RefreshToken.for_user(user).access_token

    # get the site domain
    current_site = get_current_site(request).domain

    relativeLink = reverse("email-verify")

    # activation url
    absurl = "http://" + current_site + relativeLink + "?token=" + str(token)
    email_body = (
        "Hi " + user.full_name + " Use the link below to verify your email \n" + absurl
    )

    send_mail(subject="Verify your email", to=email, email_body=email_body)

    return True


def send_email_deactivation_link(user, request, email):
    """
    Sends deactivation email to the an exist user

    Arguments:
        email (str): exist user email

    Return:
        bool: pass

    """

    # generate account activation token
    token = RefreshToken.for_user(user).access_token

    # get the site domain
    current_site = get_current_site(request).domain

    relativeLink = reverse("account-deactivate")

    # activation url
    absurl = "http://" + current_site + relativeLink + "?token=" + str(token)
    email_body = (
        "Dear customer "
        + user.full_name
        + "\n\n"
        + " Use this link below to deactivate your account: \n"
        + absurl
    )

    send_mail(subject="Deactivate account", to=email, email_body=email_body)

    return True


def send_email_password_rest(user, request, email):
    # generate account activation token
    token = RefreshToken.for_user(user).access_token

    # get the site domain
    current_site = get_current_site(request).domain

    relativeLink = reverse("forget_password_confirm")

    # activation url
    absurl = "http://" + current_site + relativeLink + "?token=" + str(token)
    email_body = (
        "Dear customer "
        + user.full_name
        + "\n\n"
        + " Use this link below to reset your account password: \n"
        + absurl
    )

    send_mail(subject="Reset Password", to=email, email_body=email_body)

    return True
