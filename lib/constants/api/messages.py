class SuccessMessage(object):
    """docstring for SuccessMessage."""

    """ ACCOUNT """
    ACCOUNT_CREATED = ""
    ACCOUNT_RETREIVED = ""
    ACCOUNT_UPDATED = ""
    ACCOUNT_ACTIVATED = ""
    ACCOUNT_DEACTIVATED = ""
    ACCOUNT_DELETED = ""

    """ AUTH """
    SIGNED_IN = ""
    SIGNED_OUT = ""

    """ FLIGHT """
    FLIGHT_RETRIEVED = ""
    FLIGHTS_RETRIEVED = ""
    FLIGHTS_PURCHASED = ""
    FLIGHTS_RETRIEVED = ""
    FLIGHTS_RETRIEVED = ""
    SIGNED_OUT = ""

    """ PAYMENT """
    PAYMENT_PROCESSED = ""

    """ PAYMENT """
    PAYMENT_PROCESSED = ""


class ErrorMessage(object):
    """docstring for ErrorMessage."""

    """ ACCOUNT """
    FAILED_TO_CREATE_ACCOUNT = ""

    """ AUTH """
    FAILED_TO_SIGN_IN = ""
    FAILED_TO_SIGN_OUT = ""

    """ FLIGHT """
    FAILED_TO_RETRIEVE_FLIGHT = ""
    FAILED_TO_PURCHASE_FLIGHT = ""
    FAILED_TO_RETRIEVE_FLIGHTS = ""
    FAILED_TO_PURCHASE_FLIGHTS = ""

    """ PAYMENT """
    FAILED_TO_PROCESS_PAYMENT = ""
