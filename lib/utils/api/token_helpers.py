# Django imports
from django.conf import settings

# external imports
import jwt


def get_token_customer_id(token):
    """Get the customer id for the decoded token"""
    payload = jwt.decode(jwt=token, key=settings.SECRET_KEY, algorithms=["HS256"])
    return payload["user_id"]


def get_jwt_token(self, request):
    """
    Get JWT token from the given request headers.
    Return:
        (token) -> if token
    """
    headers = request.headers

    if "token" in request.headers:
        return headers["token"]

    if "Authorization" in headers:
        if "Bearer" in headers["Authorization"]:
            return headers.get("Authorization")[7:]

    return None
