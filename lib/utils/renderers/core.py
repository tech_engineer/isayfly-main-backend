# external imports
from rest_framework.renderers import JSONRenderer


class RootJSONRenderer(JSONRenderer):
    def render(self, response_body, accepted_media_type=None, renderer_context=None):
        """
        Json renderer class for custom response for both succuss and fail responses
        """

        status_code = renderer_context["response"].status_code

        # set `status_message` None if `response_body` is empty,
        # else use the `get_status_message` method to extract
        # the status message
        status_message = (
            None if response_body is None else self.get_status_message(response_body)
        )
        response = None

        if str(status_code).startswith("2"):
            response = self.success_response(response_body, status_code, status_message)
        else:
            response = self.error_response(response_body, status_code, status_message)

        return super(RootJSONRenderer, self).render(
            response, accepted_media_type, renderer_context
        )

    def success_response(self, data, status_code, status_message=None):
        success_response = self.get_response_format

        success_response["message"] = "success"
        success_response["status"]["code"] = status_code
        success_response["status"]["message"] = status_message
        success_response["body"] = data

        return success_response

    def error_response(self, data, status_code, status_message=None):
        error_response = self.get_response_format

        error_response["message"] = "error"
        error_response["status"]["code"] = status_code
        error_response["status"]["message"] = status_message
        error_response["body"] = self.get_error_message(data)

        return error_response

    @property
    def get_response_format(self):
        return {
            "message": "",
            "status": {"code": "", "message": ""},
            "body": {},
        }

    def get_status_message(self, response_body):
        return (
            response_body["status_message"]
            if "status_message" in response_body
            else None
        )

    def get_error_message(self, response_body):
        if "errors" in response_body:
            return response_body["errors"]
        return response_body
